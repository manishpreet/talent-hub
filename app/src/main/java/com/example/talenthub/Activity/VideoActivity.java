package com.example.talenthub.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.talenthub.R;

public class VideoActivity extends AppCompatActivity {

    //Toolbar Instnaces
    Toolbar mToolbar;
    //Recycler View
    RecyclerView videoDataRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        mToolbar = findViewById(R.id.videoToolbar);     //locating the reference

        //Initialization of components
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Videos");

        //Recycler View
        videoDataRecyclerView = findViewById(R.id.videoRecyclerView);
        videoDataRecyclerView.setHasFixedSize(true);
        videoDataRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
