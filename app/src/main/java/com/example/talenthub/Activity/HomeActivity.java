package com.example.talenthub.Activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.talenthub.Fragment.AddFragment;
import com.example.talenthub.Fragment.FavouriteFragment;
import com.example.talenthub.Fragment.HomeFragment;
import com.example.talenthub.Fragment.ProfileFragment;
import com.example.talenthub.R;
import com.example.talenthub.Fragment.SearchFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class HomeActivity extends AppCompatActivity {

    //class instance
  //  private SectionPagerAdapter sectionPagerAdapter;
    private Toolbar mToolbar;
    private ViewPager viewPager;

    //Firebase Components
    FirebaseAuth mUserAuth;
    DatabaseReference mRef;

    //Fragment Component
    Fragment selectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        mToolbar = findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        mUserAuth = FirebaseAuth.getInstance();

       selectedFragment = HomeFragment.newInstance();
       getSupportActionBar().setTitle("Home");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame , selectedFragment);
        transaction.commit();

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.home_icon:
                        selectedFragment = HomeFragment.newInstance();
                        getSupportActionBar().setTitle("Home");
                        break;
                    case R.id.add_icon:
                        selectedFragment = AddFragment.newInstance();
                        getSupportActionBar().setTitle("Add you want to.");
                        break;
                    case R.id.search_icon:
                        selectedFragment = SearchFragment.newInstance();
                        getSupportActionBar().setTitle("Search");
                        break;
                    case R.id.like_icon:
                        selectedFragment = FavouriteFragment.newInstance();
                        getSupportActionBar().setTitle("Favourites");
                        break;
                    case R.id.profile_icon:
                        selectedFragment = ProfileFragment.newInstance();
                        getSupportActionBar().setTitle("Profile");
                        break;


                }

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame , selectedFragment);
                transaction.commit();
                return  true;
            }
        });

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame , HomeFragment.newInstance());
        transaction.commit();
    }



    }

  /*  @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser mCurrentUser = mUserAuth.getCurrentUser();

        if(mCurrentUser == null)
        {
            sendToStart();
        }

    }

    private void sendToStart() {

        Intent startIntent = new Intent(getApplicationContext() , LoginActivity.class );
        startActivity(startIntent);
        finish();

    }*/
