package com.example.talenthub.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talenthub.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    EditText uname , upass;
    Button login;
    TextView register;
    FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Edit Text
        uname = findViewById(R.id.et_username);
        upass = findViewById(R.id.et_password);

        //Button
        login = findViewById(R.id.login);
        register = findViewById(R.id.tv_register);

        //On Click Listener
         login.setOnClickListener(this);
         register.setOnClickListener(this);

         //Firebase Instances
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId()==R.id.login)
        {
            dologin();
        }
        if(view.getId()==R.id.tv_register)
        {
            Intent in= new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(in);
            finish();
        }
    }

    private void dologin(){

        //retrieving the values
        String username = uname.getText().toString();
        String password = upass.getText().toString();

        //Progress Dialog
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("Authetication");
        dialog.setMessage("We are checking your details , Wait a second .....");
        dialog.setCanceledOnTouchOutside(false);

        //checking that data is filled or not
        if (username.equals("") || password.equals("")) {

            Toast.makeText(LoginActivity.this, "Fill login details", Toast.LENGTH_SHORT).show();
        }
         else
        {
            firebaseAuth.signInWithEmailAndPassword(username , password)
                         .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                             @Override
                             public void onComplete(@NonNull Task<AuthResult> task) {

                                 if(task.isSuccessful())
                                 {
                                     Toast.makeText(getApplicationContext() , "Successfully Login" , Toast.LENGTH_SHORT).show();
                                     Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                     homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                     startActivity(homeIntent);
                                     finish();
                                 }
                                 else {

                                     Toast.makeText(getApplicationContext() , task.getException().getMessage() , Toast.LENGTH_SHORT).show();
                                 }
                             }
                         });

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
