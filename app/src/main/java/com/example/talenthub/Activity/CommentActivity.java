package com.example.talenthub.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.talenthub.Class.Comments;
import com.example.talenthub.Class.Talent;
import com.example.talenthub.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

public class CommentActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    EditText comment;
    ImageView sendcomment;
    Talent talent;
    CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        talent =new Gson().fromJson(getIntent().getStringExtra("id"),Talent.class);
        recyclerView = findViewById(R.id.recyclerView);
        adapter=new CommentAdapter(talent,this);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplication(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);


        comment=findViewById(R.id.comment);
        sendcomment=findViewById(R.id.sendcomment);

        getcomments();
       sendcomment.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               String commentData = comment.getText().toString();
               sendCommentFn(commentData);
           }
       });
    }

    private void getcomments() {
        FirebaseFirestore.getInstance().collection("Comments").whereEqualTo("talent_id", talent.getTalentId()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (DocumentSnapshot snapshot : task.getResult()) {
                                Comments comments = snapshot.toObject(Comments.class);
                                adapter.addComment(comments);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CommentActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendCommentFn(String commentData) {

        //Comment class object Initialization
        final Comments comments = new Comments(commentData, talent.getTalentId());

        FirebaseFirestore.getInstance()
                         .collection("Comments")
                         .document()
                         .set(comments)
                         .addOnCompleteListener(new OnCompleteListener<Void>() {
                             @Override
                             public void onComplete(@NonNull Task<Void> task) {

                                 if(task.isSuccessful())
                                 {
                                     adapter.addComment(comments);
                                     adapter.notifyDataSetChanged();
                                     Toast.makeText(getApplicationContext() , "Comment Uploaded" , Toast.LENGTH_SHORT).show();
                                     comment.setText("");
                                 }

                             }
                         });
    }

}
