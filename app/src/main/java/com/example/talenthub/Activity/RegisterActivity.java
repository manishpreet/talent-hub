package com.example.talenthub.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talenthub.R;
import com.example.talenthub.Class.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class RegisterActivity extends AppCompatActivity  implements View.OnClickListener  {

    EditText uname, upassword, uemail, ucontact;
    Button bsignup;
    TextView tvcancel;

    ProgressDialog progressDialog;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore firestore;
    //retrieving the values
    String name;
    String pass,email,contact;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        uname = findViewById(R.id.name);
        upassword = findViewById(R.id.pass);
        uemail = findViewById(R.id.email);
        ucontact = findViewById(R.id.contact);
        bsignup = findViewById(R.id.register);
        tvcancel = findViewById(R.id.cancel);

        bsignup.setOnClickListener(this);
       tvcancel.setOnClickListener(this);

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Wait a sec...");
        progressDialog.setCanceledOnTouchOutside(false);

        firebaseAuth=FirebaseAuth.getInstance();
        firestore=FirebaseFirestore.getInstance();


    }
    @Override
    public void onBackPressed()
    {
        Intent in=new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(in);
        finish();
    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.register) {

             name = uname.getText().toString().trim();
             email = uemail.getText().toString().trim();
            contact = ucontact.getText().toString().trim();
             pass = upassword.getText().toString().trim();
            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(contact) || TextUtils.isEmpty(email) ||
                    TextUtils.isEmpty(pass)) {

                Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show();

            } else {

                signup();
            }
        }
        else if(v.getId() == R.id.cancel)
        {
            Intent in=new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(in);
            finish();
        }
    }
    public void signup()
    {
         name=uname.getText().toString().trim();
         email=uemail.getText().toString().trim();
         contact=ucontact.getText().toString().trim();
         pass=upassword.getText().toString().trim();

        final User user= new User();
        user.setName(name);
        user.setEmail(email);
        user.setContact(contact);
        user.setPass(pass);
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                        {
                            FirebaseUser firebaseUser=task.getResult().getUser();
                            user.setUid(firebaseUser.getUid());

                            progressDialog.dismiss();

                            //display the message
                            Toast.makeText(getApplicationContext() , " Sign up done " , Toast.LENGTH_SHORT).show();
                            //call to the method
                           saveToDatabase(user);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.hide();
                Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveToDatabase(User user) {
        firestore.collection("users").document(user.getUid()).set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            /*progressDialog.hide();*/
                            Toast.makeText(RegisterActivity.this, "User Registration done", Toast.LENGTH_SHORT).show();

                            //go to the home page
                            Intent homeIntent = new Intent(getApplicationContext() , HomeActivity.class);
                            startActivity(homeIntent);
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.hide();
                Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}


