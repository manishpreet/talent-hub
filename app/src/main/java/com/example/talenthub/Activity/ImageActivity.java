package com.example.talenthub.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.talenthub.Class.ImageRecyclerViewHolder;
import com.example.talenthub.Class.User;
import com.example.talenthub.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ImageActivity extends AppCompatActivity {

    //Toolbar Instances
    Toolbar mToolbar;
    //Recycler View
    RecyclerView imageDataRecyclerView;

    //Firestore Reference
    FirebaseFirestore mRootRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        //Initialization of components
        mToolbar = findViewById(R.id.imageToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Images");

        //Firestore Reference
         mRootRef = FirebaseFirestore.getInstance();
         Query query = mRootRef.collection("users")
                 .orderBy("name" , Query.Direction.ASCENDING);

         //Firebase Recycler Options


        //Recycler View
        imageDataRecyclerView = findViewById(R.id.imageRecyclerView);
        imageDataRecyclerView.setHasFixedSize(true);
        imageDataRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Setting up the Recycler View Adapter
     /*   FirebaseRecyclerAdapter<User, ImageRecyclerViewHolder> imageRecyclerAdapter = new FirebaseRecyclerAdapter<User, ImageRecyclerViewHolder>(
                User.class,
                R.layout.image_design,
                ImageRecyclerViewHolder.class,

        ) {
            @Override
            protected void populateViewHolder(ImageRecyclerViewHolder viewHolder, User model, int position) {

            }
        };*/

    }
}