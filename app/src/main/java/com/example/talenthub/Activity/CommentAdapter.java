package com.example.talenthub.Activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.talenthub.Class.Comments;
import com.example.talenthub.Class.Talent;
import com.example.talenthub.Class.User;
import com.example.talenthub.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.Holder> {
    ArrayList<Comments>list;
    Context context;
Talent talent;
    public CommentAdapter(Talent talent, CommentActivity commentActivity){
        list=new ArrayList<>();
        this.talent=talent;
        this.context=commentActivity;
    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_comment,viewGroup,false
        );
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        holder.comment.setText(list.get(i).getMessage());
        holder.time.setText(getDate(list.get(i).getTimestemp()));
        FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            User user=task.getResult().toObject(User.class);
                            Glide.with(context)
                                    .load(user.getImage())
                                    .into(holder.profile);
                            Glide.with(context)
                                    .load(user.getImage())
                                    .into(holder.profile);
                        }
                    }
                });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addComment(Comments comments) {
        list.add(comments);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {
        CircleImageView profile;
        TextView comment;
        TextView time;
        public Holder(@NonNull View itemView) {
            super(itemView);
            comment=itemView.findViewById(R.id.comment);
            time=itemView.findViewById(R.id.timestemp);
            profile=itemView.findViewById(R.id.profile);
        }
    }
    String getDate(Long time){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date(time));
        return dateString;
    }
}

