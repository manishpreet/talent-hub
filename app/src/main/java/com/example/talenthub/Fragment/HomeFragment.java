package com.example.talenthub.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.talenthub.Activity.ImageActivity;
import com.example.talenthub.Activity.VideoActivity;
import com.example.talenthub.Class.Talent;
import com.example.talenthub.Class.User;
import com.example.talenthub.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;


public class HomeFragment extends Fragment implements ImageAdapter.OnClick {

    //Initalization of components
    View mMainView;

    Button mImageBtn;
    Button mVideoBtn;
    ImageAdapter adapter;
    RecyclerView recyclerView;
    FirebaseAuth firebaseAuth;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static Fragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mMainView = inflater.inflate(R.layout.fragment_home, container, false);


        return mMainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        //locating the reference
        //    mImageBtn = mMainView.findViewById(R.id.imageBtn);
        //     mVideoBtn = mMainView.findViewById(R.id.videoBtn);
        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        adapter = new ImageAdapter(getActivity(), this);
        recyclerView.setAdapter(adapter);
        getData();

   /*     mImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageData();

            }
        });

        mVideoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                videoData();
                }
        });*/
    }


    private void getData() {
        FirebaseFirestore.getInstance().collection("talents").orderBy("timestemp", Query.Direction.DESCENDING).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult().size() > 0) {
                            for (int i = 0; i < task.getResult().size(); i++) {
                                Talent talent = task.getResult().getDocuments().get(i).toObject(Talent.class);
                                talent.setTalentId(task.getResult().getDocuments().get(i).getId());
                                adapter.add(talent);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void videoData() {

        Intent videoIntent = new Intent(getContext(), VideoActivity.class);
        startActivity(videoIntent);

    }

    private void imageData() {

        Intent videoIntent = new Intent(getContext(), ImageActivity.class);
        startActivity(videoIntent);
    }


    @Override

    public void openDialog(Talent talent) {
        VideoDialog.instance(talent).show(getFragmentManager(), VideoDialog.class.getName());
    }
}





