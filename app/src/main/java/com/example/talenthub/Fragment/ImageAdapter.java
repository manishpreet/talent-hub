package com.example.talenthub.Fragment;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.talenthub.Activity.CommentActivity;
import com.example.talenthub.Activity.HomeActivity;
import com.example.talenthub.Class.Talent;
import com.example.talenthub.Class.User;
import com.example.talenthub.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> {

      ArrayList<Talent>list;
      OnClick listner;
      Context context;

    public ImageAdapter(Context homeFragment,OnClick listner) {
        list=new ArrayList<>();
        context=homeFragment;
        this.listner =listner;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_design, viewGroup,false);
        return new Holder(view);
    }

    String getDate(Long time){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = formatter.format(new Date(time));
       return dateString;
    }
    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        final Talent talent=list.get(i);
        holder.date.setText(getDate(talent.getTimestemp()));
        if (talent.getType().equals("image"))
        {
            Glide.with(context)
                    .load(talent.getImageLink())
                    .into(holder.imageImage);
            Glide.with(context)
                    .load(talent.getImageLink())
                    .into(holder.imageBg);
            holder.imageVideo.setVisibility(View.GONE);

        }else {
            holder.imageVideo.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .asBitmap()
                    .load(talent.getVideoLink())
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .into(holder.imageImage);
            Glide.with(context)
                    .load(talent.getVideoLink())
                    .into(holder.imageBg);
        }
        FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            User user=task.getResult().toObject(User.class);
                            holder.name.setText(user.getName());
                            Glide.with(context)
                                    .load(user.getImage())
                                    .into(holder.profilePic);
                            Glide.with(context)
                                    .load(user.getImage())
                                    .into(holder.commentPic);
                        }
                    }
                });
        holder.imageImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               listner.openDialog(talent);
            }
        });
        holder.commentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tale=new Gson().toJson(talent);
                Intent intent=new Intent(context,CommentActivity.class);
                intent.putExtra("id",tale);
                context.startActivity(intent);
            }
        });

        holder.saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Favourite favourite=new Favourite(talent.getTalentId());
                FirebaseFirestore.getInstance().collection("Favourites")
                        .document(FirebaseAuth.getInstance().getUid()).collection("favoutite")
                        .document(talent.getTalentId()).set(favourite);
            }
        });

    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public void add(Talent talent) {
        list.add(talent);
        notifyDataSetChanged();
    }


    public class Holder extends RecyclerView.ViewHolder {
        ImageView imageImage,commentImage;
        ImageView imageVideo;
        ImageView imageBg;
        ImageView saveImage;
        CircleImageView profilePic;
        CircleImageView commentPic;
        TextView name;
        TextView date;
        public Holder(@NonNull android.view.View itemView) {
            super(itemView);
            imageImage=itemView.findViewById(R.id.imageImage);
            saveImage=itemView.findViewById(R.id.saveImage);
            imageVideo=itemView.findViewById(R.id.imageVideo);
            imageBg=itemView.findViewById(R.id.imageBg);
            profilePic=itemView.findViewById(R.id.imageProfileUser);
            name=itemView.findViewById(R.id.imageUserName);
            date=itemView.findViewById(R.id.date);
            commentPic = itemView.findViewById(R.id.commentPic);
            commentImage = itemView.findViewById(R.id.commentImage);

        }

    }
    interface OnClick
    {
        void openDialog(Talent talent);
    }
}

