package com.example.talenthub.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;
import com.bumptech.glide.Glide;
import com.example.talenthub.Class.Talent;
import com.example.talenthub.R;

public class VideoDialog extends DialogFragment {
    static Talent talent;

    public static VideoDialog instance(Talent tal)
    {
        talent=tal;
        return new VideoDialog();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mMainView = inflater.inflate(R.layout.fragment_video_dialog, container, false);
         return mMainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView button = view.findViewById(R.id.img);
        VideoView video = view.findViewById(R.id.videoView);


        if (talent.getType().equals("image"))
       {
           Glide.with(getActivity())
                   .load(talent.getImageLink())
                   .into(button);
           video.setVisibility(View.GONE);
       }else {
           button.setVisibility(View.GONE);
           Uri uri = Uri.parse(talent.getVideoLink());
           video.setVideoURI(uri);
           video.start();
       }
    }

}

