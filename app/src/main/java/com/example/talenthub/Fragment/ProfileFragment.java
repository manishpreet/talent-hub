package com.example.talenthub.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.talenthub.Activity.LoginActivity;
import com.example.talenthub.Class.User;
import com.example.talenthub.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileFragment extends Fragment {

    TextView logout,username,name,email,contact;
    CircleImageView profileimage;
    ImageView imagebg;
    int REQUEST_CODE=12;
    private StorageReference mStorageRef;

    //Firebase Instances
    FirebaseAuth mUserAuth;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }
    @Override
    public void onResume() {
        super.onResume();
        getUser();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUserAuth = FirebaseAuth.getInstance();
        logout=view.findViewById(R.id.logout1);
        mStorageRef= FirebaseStorage.getInstance().getReference();
        profileimage=view.findViewById(R.id.profileimage);
        imagebg=view.findViewById(R.id.imgbg);

        profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imagerPicker();
            }

        });


        //Firebase Instances
        mUserAuth = FirebaseAuth.getInstance();
        username=view.findViewById(R.id.username);
        name=view.findViewById(R.id.name);
        email=view.findViewById(R.id.email);
        contact=view.findViewById(R.id.contact);



        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserAuth.signOut();
                Intent loginIntent = new Intent(getContext() , LoginActivity.class);
                startActivity(loginIntent);

            }
        });

        getUser();
    }

    private void imagerPicker() {
        Intent in = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(in, REQUEST_CODE);
    }


    private void getUser() {

            FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                User user = task.getResult().toObject(User.class);
                                Glide.with(getContext()).load(user.getImage())
                                        .placeholder(R.drawable.plm1)
                                        .into(profileimage);
                                Glide.with(getContext())
                                        .load(user.getImage())
                                        .placeholder(R.drawable.plm1)
                                        .into(imagebg);
                                username.setText(user.getName());
                                name.setText("Name :"+ user.getName());
                                email.setText("Email :"+ user.getEmail());
                                contact.setText("Contact :"+ user.getContact());


                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            Uri path = data.getData();
            uploadImage(path);
        }
    }

    private void uploadImage(Uri path) {
        final StorageReference ref = mStorageRef.child("users/" + FirebaseAuth.getInstance().getUid() + ".jpg");
        UploadTask uploadTask = ref.putFile(path);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Toast.makeText(getContext(), downloadUri.toString(), Toast.LENGTH_SHORT).show();
                    updateProfile(downloadUri);
                }

            }
        });
    }

            private void updateProfile(final Uri downloadUri) {
                FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getUid())
                        .update("image", downloadUri.toString())
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Glide.with(getActivity())
                                .load(downloadUri)
                                .into(profileimage);
                        Glide.with(getActivity())
                                .load(downloadUri)
                                .into(imagebg);
                    }
                });
            }


            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                // Inflate the layout for this fragment
                View view = inflater.inflate(R.layout.fragment_profile, container, false);
                return view;
            }
        }


