package com.example.talenthub.Class;

import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

public class Comments {
    String message;
    String uid;

    public String getTalent_id() {
        return talent_id;
    }

    public void setTalent_id(String talent_id) {
        this.talent_id = talent_id;
    }

    String talent_id;
    Long timestemp;

    public Comments() {
    }

    public Comments(String m, String talentId) {
        this.message = m;
        this.talent_id=talentId;
        this.uid = FirebaseAuth.getInstance().getUid();
        this.timestemp = new Date().getTime();

    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }
    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getTimestemp() {
        return timestemp;
    }
    public void setTimestemp(Long timestemp) {
        this.timestemp = timestemp;
    }
}
