package com.example.talenthub.Class;

import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

public class Talent {
    String description;
    String uid;

    public String getTalentId() {
        return talentId;
    }

    public void setTalentId(String talentId) {
        this.talentId = talentId;
    }
    public Talent(){

    }

    String talentId;

    public Talent(Uri downloadUri, String s,String type,String talentId) {
        this.description=s;
        this.uid= FirebaseAuth.getInstance().getUid();
        this.timestemp=new Date().getTime();
        this.talentId=talentId;

        this.type=type;
        if (type.equals("image"))
            imageLink=downloadUri.toString();
        else
            videoLink=downloadUri.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getTimestemp() {
        return timestemp;
    }

    public void setTimestemp(Long timestemp) {
        this.timestemp = timestemp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    Long timestemp;
    String type;
    String imageLink;
    String videoLink;
}
