package com.example.talenthub.Class;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.talenthub.Fragment.AddFragment;

public class SectionPagerAdapter extends FragmentPagerAdapter {

    public SectionPagerAdapter(FragmentManager fm)
    {
       super(fm);
    }
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0 :
                return null;
            case 1:
                return  null;
            case 2:
                AddFragment addFragment = new AddFragment();
                return  addFragment;

        }

        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}
